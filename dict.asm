%include "lib.inc"

global find_word

section .text

; находит элемент словаря по ключу и возвращает его в rax
find_word:
    .loop:
    cmp rdi, 0 
    je .bad
    push rdi 
    add rdi, 8 
    call string_equals 
    pop rdi 
    cmp rax, 0 
    jne .good
    mov rdi, [rdi] 
    jmp .loop
    .good:
    mov rax, rdi 
    ret
    .bad:
    xor rax, rax 